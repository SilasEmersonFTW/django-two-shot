from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "account_list": accounts,
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect('account_list')
    form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": categories,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect('category_list')
    form = ExpenseForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "home": receipts,
    }
    return render(request, "receipts/homepage.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect('home')
    form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)
