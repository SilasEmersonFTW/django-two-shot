from django.urls import path
from receipts.views import home, create_receipt, category_list, account_list
from receipts.views import create_category, create_account

urlpatterns = [
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/", category_list, name="category_list"),
    path("categories/create/", create_category, name="create_category"),
    path("create/", create_receipt, name="create_receipt"),
    path("", home, name="home"),
]
